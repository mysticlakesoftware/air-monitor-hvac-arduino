# Air Monitor HVAC Arduino

This is the Arduino source code for a project on Hackster.io submitted for the competition [IoT Without Limits](https://www.hackster.io/contests/Helium/projects). The 
project itself is [Air Environment Monitoring](https://www.hackster.io/rudmerriam/air-environment-monitoring-91dcbf).

This project monitors two situations, general air monitoring and HVAC (heating, ventilation, and air-conditioning) systems. The collected data is sent via the Helium infrastructure to the Google Cloud IoT 
for data storage and analysis. Another repository contains the code for the [cloud](https://bitbucket.org/mysticlakesoftware/air-monitor-hvac-cloud/src/master/).

## Overview

The system uses an Arduino Due board to obtain the data from the sensors. An LCD provides a visual indication of the data. The data collected is temperature, relative humidity, atmospheric pressure, volatile organize compounds,
and carbon dioxide (CO2). 

For general environment monitoring only one set of sensors is used. 

For HVAC monitoring, two sets are used with one at the inflow and one at the outflow of the HVAC system. The HVAC system uses two sets of sensors to provide differential data which can indicate issues with the 
HVAC that might need to be addressed. Actually, there are other applications where dual sensors are useful. One is checking a refrigerator with one set of sensors inside and the other outside. 

One additional sensor appears in the code for monitoring particulate matter by the measuring the amount of reflected of LED light. This is not fully implemented since there is no software library available to analyze the reflectance.

The sensors used are:

* Bme 280 - presuure, relative humidity
* Ccs811 - VOC (volatile organice compounds) , CO2 derived from the VOC measurement
* Tmp102 - temperature
* Max30105 - particulates

The other components of the system are an 16x2 LCD display and the Helium Atom. See the project story on Hackster.io for additional information on the sensors and components. 

## C++ Classes

The general approach for the system is to wrap the existing Arduino libraries for the sensors and components with classes to facilitate the project development. This system diagram shows these classes.

![System Classes](images/Classes.jpg "System Classes")

The Application class contains all these classes and implements methods `setup()` and `loop()` that are simply called by the standard Arduino functions of those names. 

## I2C Issues
The original code was for an Arduino Uno, actually a Sparkfun RedBoard clone, but when all the code was merged the system crashed at startup. Presumably the memory requirements exceeds the Uno's capabilities. 
An Arduino Due replaced the Uno. 

The Arduino Wire library uses the A4/A5 pins on the Uno. These are also wired separately on the Uno board to SDA/SCL. This default usage on the Uno is the *Wire* instance of the class. On the Due, *Wire* is on
different pins and *Wire1* is on the SDA/SCL pins. The A4/A5 pins on the Due are not used for I2C. 

The libraries for the BME280, LCD, and MAX3015 allow changing the I2C port from *Wire* to *Wire1*. The libaries for the CCS811 and TMP102 do not allow this. These libraries were edited to use *Wire1* by replacing *Wire*. 
This edit will need to be done for the code to work.
