//======================================================================================================================
// Display.cpp
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
#include <Streaming.h>

#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>

#include "Display.h"
//---------------------------------------------------------------------------------------------------------------------
static uint8_t deg_data[8] { 0xc, 0x12, 0x12, 0xc, 0x0, 0x0, 0x0, 0 };  // character to show degree character
const char deg { 7 };

//---------------------------------------------------------------------------------------------------------------------
Display::LcdPause::LcdPause(Display& d) :
    mDisplay(d) {
    mDisplay.mLcd.noDisplay();
}
//---------------------------------------------------------------------------------------------------------------------
Display::LcdPause::~LcdPause() {
    mDisplay.mLcd.display();
}
//---------------------------------------------------------------------------------------------------------------------
void Display::begin() {
    mLcd.begin(16, 2);

    LcdPause l_pause( *this);
    mLcd.createChar(7, deg_data);
    mLcd.home();

    mLcd << "Starting...";
    mLcd.setCursor(0, 1);
    mLcd << "please wait...";
    mLcd.setBacklight(WHITE);
    mLcd.setCursor(0, 0);
}
//---------------------------------------------------------------------------------------------------------------------
void Display::clear() {
    mLcd.clear();
}
//---------------------------------------------------------------------------------------------------------------------
void Display::dataInit() {
    LcdPause l_pause( *this);

    mLcd.clear();
    mLcd.home();
    mLcd << "S.: " << "...." << deg << " " << "...." << "mb";

    mLcd.setCursor(0, 1);
    mLcd << ".." << "% " << ".." << "%C " << ".." << "%V";
}
//======================================================================================================================
//  utility class to right justify data in field
template <typename T>
class right {
public:
    right(const T& value, const uint8_t width) :
        value(value), width(width) {
    }

    const T& value;
    const uint8_t width;
};
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
Print& operator<<(Print& p, const right<T>& r) {
    switch (r.width) {
        case 0:
        case 1:
        default:
            break;
        case 2:
            if (r.value < 10) {
                p << ' ';
            }
            break;
    }
    p << r.value;
    return p;
}
//---------------------------------------------------------------------------------------------------------------------
void Display::dataUpdate(const char unit, const float temp, const uint32_t prs, const uint32_t humidity, //
    const uint32_t co2, const uint32_t voc) {

    mLcd.setCursor(1, 0);
    mLcd << unit;

    mLcd.setCursor(4, 0);
    mLcd << _FLOAT(round(temp * 10.0) / 10.0, 1);

    mLcd.setCursor(10, 0);
    mLcd << prs;

    mLcd.setCursor(0, 1);
    mLcd << right<uint32_t>(humidity, 2);

    mLcd.setCursor(4, 1);
    mLcd << "  ";

    mLcd.setCursor(4, 1);
    mLcd << right<uint32_t>(co2, 2);

    mLcd.setCursor(9, 1);
    mLcd << right<uint32_t>(voc, 2);
}
