//======================================================================================================================
// Application.cpp
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================

#include <vector>
using namespace std;

#include <Streaming.h>
#include <Wire.h>

#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>
#include <SparkFunCCS811.h>
#include <SparkFunBME280.h>
#include <SparkFunTMP102.h>
#include <MAX30105.h>

#include <Helium.h>

#include "SensorAbstract.h"

#include "Bme280.h"
#include "Ccs811.h"
#include "Display.h"
#include "HeliumComm.h"
#include "Max3015.h"
#include "Tmp102.h"

#include "Application.h"

Application app;
/*/----------------------------------------------------------------------------------------------------------------------
 * A vector containing the pointers to the possible sensors in the application. This allows walking through the
 * vector to execute virtual functions declared the base class SensorAbstract. Actual implementation is in the sensor
 * class is executed.
 */
vector<SensorAbstract*> Application::mSensors = { //
    &app.mBme280_1, //
        &app.mCcs811_1, //
        &app.mTmp102_1, //
        &app.mMax30105, //
        &app.mBme280_2, //
        &app.mCcs811_2, //
        &app.mTmp102_2, //
    };
/*======================================================================================================================
 * The standard Arduino setup and loop code. They just call the Application methods which do the actual work.
 */
void setup() {
    app.setup();
}
//----------------------------------------------------------------------------------------------------------------------
void loop() {
    app.loop();
}
//======================================================================================================================
auto Application::setup() -> void {

    Serial.begin(115200);
    if (Serial) {
        Serial << "Starting up, please wait." << endl;
    }

    // using Wire1 on the Due. Uno would use Wire. See note about editing source for some sensors to use Wire1.
    Wire1.begin();
    Wire1.setClock(400000);

    mLcd.begin();

    mCcs811_1.begin(Ccs811::TenSec);
    mCcs811_2.begin(Ccs811::TenSec);

    mMax30105.begin(Max3015::e12inch, Max3015::eNone, Max3015::eRedIrGrn, Max3015::e50, Max3015::e411Millis, Max3015::e16384);

    mTmp102_1.begin();
    mTmp102_2.begin();

    mBme280_1.begin(Bme280::normal, Bme280::milli1000, Bme280::hum_off, Bme280::one, Bme280::one, Bme280::one);
    mBme280_2.begin(Bme280::normal, Bme280::milli1000, Bme280::hum_off, Bme280::one, Bme280::one, Bme280::one);

    mHelium.begin();

    mLcd.dataInit();
}
//---------------------------------------------------------------------------------------------------------------------
auto Application::buildHeMsg() -> String {

    Serial.println((long unsigned int)mHelium.mac_addr());

    String msg { mHelium.cur_time() };
    msg += " ";
    msg += String((long unsigned int)mHelium.mac_addr()) + " ";
    msg += String(mTmp102_1.temperatureF(), 1) + " ";
    msg += String(round(mBme280_1.pressure())) + " ";
    msg += String(round(mBme280_1.humidity())) + " ";
    msg += String(mCcs811_1.co2_scaled()) + " ";
    msg += String(mCcs811_1.voc_scaled());

    // if the second BME doesn't exist assume none of the second sensors exist, i.e. handheld mode
    // the code on the Google IOT will handle this by detecting the difference in message size
    if (mBme280_2.exists()) {
        msg += " ";
        msg += String(mTmp102_2.temperatureF(), 1) + " ";
        msg += String(round(mBme280_2.pressure())) + " ";
        msg += String(round(mBme280_2.humidity())) + " ";
        msg += String(mCcs811_2.co2_scaled()) + " ";
        msg += String(mCcs811_2.voc_scaled());
    }

    return msg;
}
//----------------------------------------------------------------------------------------------------------------------
//  The CCS811 gives better results if given the temperature and humidity. It's own temperature reads high so using
//  the TMP102 temperature
auto Application::updateCcsEnvironment() -> void {
    mCcs811_1.setEnvironmentalData(mBme280_1.humidity(), mTmp102_1.temperatureC());
    mCcs811_2.setEnvironmentalData(mBme280_2.humidity(), mTmp102_2.temperatureC());
}
//----------------------------------------------------------------------------------------------------------------------
//  Walk the vector to read each sensor - readSensor checks for existence
auto Application::readSensors(const timer_t m) -> bool {

    if (long(m - mSensorUpdate) >= 0) {
        mSensorUpdate += secondToTimer(sensor_interval);
        for (SensorAbstract* s : Application::mSensors) {
            readSensor( *s);
        }

        updateCcsEnvironment();
        return true;
    }
    return false;
}
//----------------------------------------------------------------------------------------------------------------------
//  Walk the vector to write each sensor to the terminal - reportSensor checks for existence
auto Application::reportSensors(const timer_t m) -> bool {

    if (long(m - mTerminalUpdate) >= 0) {
        mTerminalUpdate += secondToTimer(terminal_interval);

        if (Serial) {

            for (SensorAbstract* s : Application::mSensors) {
                reportSensor( *s);
                Serial << tab;
            }
            Serial.println();
        }
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
//  Display current readings on LCD
auto Application::updateDisplay(const timer_t m) -> bool {

    static uint8_t display_s1_or_s2_ { 1 };

    if (long(m - mDisplayUpdate) >= 0) {
        mDisplayUpdate += secondToTimer(display_interval);

        switch (display_s1_or_s2_) {
            case 1:
                if (mBme280_1.exists()) {
                    mLcd.dataUpdate('1', mTmp102_1.temperatureF(), mBme280_1.pressureMB(), mBme280_1.humidity(), //
                    mCcs811_1.co2_scaled(), mCcs811_1.voc_scaled());
                }
                break;

                // if second BME doesn't exist the display won't change
            case 2:
                if (mBme280_2.exists()) {
                    mLcd.dataUpdate('2', mTmp102_2.temperatureF(), mBme280_2.pressureMB(), mBme280_2.humidity(), //
                    mCcs811_2.co2_scaled(), mCcs811_2.voc_scaled());

                }
                break;
        }
        display_s1_or_s2_ = 3 - display_s1_or_s2_;
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
//  build the message to send via the Helium Atom
//  a sequence of values separated by a space. Parsed in the node.js function that receives the data
auto Application::msgSend() -> void {

    if (mHelium.isConnected()) {    // connected to Helium cloud so send data
        String msg { buildHeMsg() };
        if (Serial) {
            Serial << msg << endl;
        }
        mHeliumStatus = mHelium.send(msg.c_str());
        mLcd.setBacklight(Display::WHITE);
    }
    else { // error of some kind so try restart everything for next pass
        mHelium.begin();
        mLcd.setBacklight(Display::RED);    // indicate problem via LCD
    }
}
//---------------------------------------------------------------------------------------------------------------------
//  send current sensor data to Google IOT via Helium
auto Application::cloudSend(const timer_t m) -> bool {

    if (long(m - mHeliumUpdate) >= 0) {
        mHeliumUpdate += secondToTimer(helium_interval);
        msgSend();
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
//  had a problem with Helium connection so try to reset it. Also in case Atom got new firmware download
auto Application::heliumReset(const timer_t m) -> bool {

    if (long(m - mHeliumReset) >= 0) {
        mHeliumReset += secondToTimer(he_reset_interval);
        if (mHelium.needsReset()) {
            mHelium.reset();
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
// do all that needs to be done at the designated times
auto Application::loop() -> void {

    //  prototype for member functions to execute
    //  the timer is checked and reset by the function itself
    using activity = bool (Application::*)(const timer_t m);

    //  the member functions called by the scheduler
    static vector<activity> activities { //
    &Application::updateDisplay, //
        &Application::readSensors, //
        &Application::reportSensors, //
        &Application::cloudSend, //
        &Application::heliumReset };

    //  not doing anything else with buttons so let's trigger a send for testing if one is pushed
    uint8_t buttons = mLcd.readButtons();
    if (buttons) {
        msgSend();
    }

    timer_t m = micros();

    // walk through the list of activities to allow them to run if it is their time
    for (activity a : activities) {
        if (((this)->*a)(m)) {
            m = micros();   // reset time so later members that trigger set their restart time accurately
        }
    }
}

