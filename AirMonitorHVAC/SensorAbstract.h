#ifndef SENSORABSTRACT_H_
#define SENSORABSTRACT_H_
//======================================================================================================================
// SensorAbstract.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
constexpr char tab { '\t' };    // hung this here as a convenient place. Used in print routines to provide spacing

//----------------------------------------------------------------------------------------------------------------------
//  Class to define a common interface for all the sensors.
//      exists() - is the sensor connected to I2C bus
//      isReady() - is there data ready from the sensor. Most don't need this.
//      read() - get the data (duh!)
//      print() - print the data to serial port
//----------------------------------------------------------------------------------------------------------------------
class SensorAbstract {
public:

    SensorAbstract(const char* tag = "xx");
    virtual ~SensorAbstract() = 0;

    bool exists() const;
    virtual bool isReady() = 0;

    virtual bool read() = 0;
    virtual void print() const;

protected:
    bool mExists { false };
    const char* mTag;
};
//---------------------------------------------------------------------------------------------------------------------
inline SensorAbstract::SensorAbstract(const char* tag) :
    mTag(tag) {
}
//---------------------------------------------------------------------------------------------------------------------
inline SensorAbstract::~SensorAbstract() {
}
//---------------------------------------------------------------------------------------------------------------------
inline bool SensorAbstract::exists() const {
    return mExists;
}
//---------------------------------------------------------------------------------------------------------------------
inline void SensorAbstract::print() const {
    Serial << mTag << ' ';
}

#endif /* SENSORABSTRACT_H_ */

