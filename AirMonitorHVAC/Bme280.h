#ifndef BME280_H_
#define BME280_H_
//======================================================================================================================
// Bme280.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================

class Bme280 : public SensorAbstract {
public:
    enum I2CAddr { //
        BME280_1 = 0x77, BME280_2 = 0x76
    };
    enum Standby {
        micro500 = 0, micro62500, milli125, milli250, milli500, milli1000, milli10, milli20
    };
    enum Mode {
        sleep = 0, forced, normal = 3
    };
    enum HumFilter {
        hum_off = 0, hum_two, hum_four, hum_eight, hum_sixteen
    };
    enum OverSample {
        off = 0, one, two, four, eight, sixteen
    };

    Bme280(const char* tag, const I2CAddr i2c_addr = BME280_1);
    virtual ~Bme280() = default;

    bool begin(const Mode mode, const Standby standby, const HumFilter hum, const OverSample temp, const OverSample prs,
        const OverSample humidity);

    bool isReady() override;

    void reset();

    virtual bool read() override;
    virtual void print() const override;

    float temperatureC() const;
    float temperatureF() const;
    float pressure() const;
    float pressureMB() const;
    float humidity() const;

private:
    BME280 mBme280;
    const I2CAddr mI2cAddr;

    float mTempC { };
    float mTempF { };
    float mPressure { };
    float mHumidity { };
};
//---------------------------------------------------------------------------------------------------------------------
inline bool Bme280::isReady() {
    if (mExists) {
        return !mBme280.isMeasuring();
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
inline void Bme280::reset() {
    mBme280.reset();
}
//---------------------------------------------------------------------------------------------------------------------
inline float Bme280::temperatureC() const {
    return mTempC;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Bme280::temperatureF() const {
    return mTempF;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Bme280::pressure() const {
    return mPressure;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Bme280::pressureMB() const {
    return mPressure / 100.0;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Bme280::humidity() const {
    return mHumidity;
}
//---------------------------------------------------------------------------------------------------------------------
inline bool Bme280::read() {
    if (isReady()) {
        mTempC = mBme280.readTempC();
        mTempF = mBme280.readTempF();
        mPressure = mBme280.readFloatPressure();
        mHumidity = round(mBme280.readFloatHumidity());
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
inline void Bme280::print() const {
    SensorAbstract::print();
    Serial << _FLOAT(temperatureF(), 1) << tab  //
        << round(pressureMB()) << tab  //
        << round(humidity()) << tab;
}

#endif /* BME280_H_ */
