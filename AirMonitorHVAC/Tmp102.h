#ifndef TMP102_H_
#define TMP102_H_
//======================================================================================================================
// Tmp201.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================

class Tmp102 : public SensorAbstract {
public:
    // Sensor address can be changed with an external jumper to:
    // ADD0 - Address
    //  VCC - 0x49
    //  SDA - 0x4A
    //  SCL - 0x4B
    enum I2CAddr { //
        TMP102_1 = 0x48, TMP102_2, TMP102_3, TMP102_4
    };

public:
    Tmp102(const char* tag, const I2CAddr i2c_add);

    void begin();
    virtual bool read() override;
    virtual void print() const override;

    float temperatureC() const;
    float temperatureF() const;

    virtual bool isReady() override;

private:
    const I2CAddr mI2cAddr;
    TMP102 mTmp102;

    float mTempC { };
};
//---------------------------------------------------------------------------------------------------------------------
inline float Tmp102::temperatureC() const {
    return mTempC;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Tmp102::temperatureF() const {
    return mTempC * 9.0 / 5.0 + 32.0;
}
//---------------------------------------------------------------------------------------------------------------------
inline bool Tmp102::isReady() {
    return exists();
}
//---------------------------------------------------------------------------------------------------------------------
inline void Tmp102::print() const {
    SensorAbstract::print();
    Serial << temperatureF() << tab;
}

#endif /* TMP102_H_ */
