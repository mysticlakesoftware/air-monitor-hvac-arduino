#ifndef DISPLAY_H_
#define DISPLAY_H_
//======================================================================================================================
// Display.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
class Display {
public:

    //======================================================================================================================
    struct LcdPause {   // automatically toggle lcd off and on when updating
        // in C++ 11 a nested class should have access to the outer class members. AVR may not support this.

        LcdPause(Display& d);
        ~LcdPause();

        Display& mDisplay;
    };

    enum I2CAddr {
        LCD = 0x20
    };

    enum Background {
        RED = 0x1, GREEN = 0x2, YELLOW = 0x3, BLUE = 0x4, VIOLET = 0x5, TEAL = 0x6, WHITE = 0x7
    };

    Display();


    void begin();
    void clear();
    void setBacklight(const Background color);
    uint8_t readButtons();

    void dataInit();
    void dataUpdate(const char unit, const float temp, const uint32_t prs, const uint32_t humidty, const uint32_t co2, const uint32_t voc);

protected:
    Adafruit_RGBLCDShield mLcd;
};
//---------------------------------------------------------------------------------------------------------------------
inline Display::Display() :
    mLcd { } {
}
//---------------------------------------------------------------------------------------------------------------------
inline void Display::setBacklight(const Background color) {
    mLcd.setBacklight(color);
}
//---------------------------------------------------------------------------------------------------------------------
inline uint8_t Display::readButtons() {
    return mLcd.readButtons();
}

#endif /* DISPLAY_H_ */
