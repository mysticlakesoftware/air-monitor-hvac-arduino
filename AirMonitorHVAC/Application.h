#ifndef APPLICATION_H_
#define APPLICATION_H_
//======================================================================================================================
// Application.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
/*======================================================================================================================
 *
 * The Application class is invoked by the standard Arduino setup and loop calls which reside at the top of the
 * file Application.cpp. This Application class wraps all the classes normally created at global scope which allows
 * control over their order of creation and initialization.
 *
 * #### Note ###
 * The size of the code and / or memory space required using a Arduino Due processor. As a result the I2C port uses
 * different pins than the Uno. This is defined as Wire1 in the Wire library. Unfortunately the libraries for some
 * of the sensors are hard coded for the standard Wire port. The code was edited in these libraries to Wire1. Not a
 * good way to handle this but best that could be done without a rewrite.
 *
 * Note: check for Adafruit or other libraries that support these sensors
 *
 * ** The libraries that do not requires changes are the BME280, LCD, and MAX30105
 *
 * ** The libraries that do require changes are CCS811 and TMP102. Do a search and replace of Wire with Wire1 for them.
 ======================================================================================================================*/
class Application {
public:
    using timer_t = unsigned long;

    // intervals for the execution of the members run by the scheduler, in seconds
    static constexpr float display_interval { 2.5 };
    static constexpr float sensor_interval { display_interval * 2 };
    static constexpr float terminal_interval { 10 };
    static constexpr float helium_interval { 30 };
    static constexpr float he_reset_interval { 60 * 8 }; // 8 hours

    Application() = default;

    auto setup() -> void;
    auto loop() -> void;

private:
    auto readSensors(const timer_t m) -> bool;
    auto reportSensors(const timer_t m) -> bool;

    // template members which read any sensor derived from SensorAbstract
    template <typename T> auto readSensor(T& sensor) -> void;
    template <typename T> auto reportSensor(T& sensor) -> void;

    // build the message to send over Helium to the Google IOT
    auto buildHeMsg() -> String;

    // set temperature and humidity for the CCS811 sensor to get better results
    void updateCcsEnvironment();

    Display mLcd;

    HeliumComm mHelium;
    int mHeliumStatus { 0 };

    Bme280 mBme280_1 { "B1", Bme280::BME280_1 };
    Bme280 mBme280_2 { "B2", Bme280::BME280_2 };
    Ccs811 mCcs811_1 { "C1", Ccs811::CCS811_1 };
    Ccs811 mCcs811_2 { "C2", Ccs811::CCS811_2 };
    Max3015 mMax30105 { "M1" };
    Tmp102 mTmp102_1 { "T1", Tmp102::TMP102_1 };
    Tmp102 mTmp102_2 { "T2", Tmp102::TMP102_2 };

    static std::vector<SensorAbstract*> mSensors;

    // timers for determining when output takes place.
    timer_t secondToTimer(const float& period) const {
        return period * 1000000;
    }

    bool updateDisplay(const timer_t m);
    bool cloudSend(const timer_t m);
    bool heliumReset(const timer_t m);
    void msgSend();

    //  initialize timers for scheduling
    timer_t mDisplayUpdate { secondToTimer(display_interval) + micros() };
    timer_t mSensorUpdate { secondToTimer(sensor_interval) + micros() };    // read every other LCD update
    timer_t mTerminalUpdate { secondToTimer(terminal_interval) + micros() };
    timer_t mHeliumUpdate { secondToTimer(helium_interval) + micros() };
    timer_t mHeliumReset { secondToTimer(he_reset_interval) + micros() };
};
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
inline auto Application::reportSensor(T& sensor) ->void {
    if (sensor.exists()) {
        sensor.print();
    }
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
inline auto Application::readSensor(T& sensor) -> void {
    if (sensor.exists()) {
        sensor.read();
    }
}

#endif /* APPLICATION_H_ */
