//======================================================================================================================
// Ccs811.cpp
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
#include <Streaming.h>

#include <SparkFunCCS811.h>

#include "SensorAbstract.h"
#include "Ccs811.h"
//---------------------------------------------------------------------------------------------------------------------
void Ccs811::begin(const Mode mode)
{
    mExists = CCS811Core::SENSOR_SUCCESS == mCcs811.begin();
    if (mExists) {
        mCcs811.setDriveMode(mode);
        mCcs811.setRefResistance(10000);
    }
}
//---------------------------------------------------------------------------------------------------------------------
bool Ccs811::isDataAvailable() {
    bool is_ready { mCcs811.dataAvailable() };

    if (is_ready) {
        mCcs811.readAlgorithmResults();
        mVoc = mCcs811.getTVOC();
        mCo2 = mCcs811.getCO2();

        if (mCcs811.readNTC() == CCS811::SENSOR_SUCCESS) {
            mTempC = mCcs811.getTemperature();
        }
    }
    return is_ready;
}
//---------------------------------------------------------------------------------------------------------------------
float Ccs811::temperatureC() {
    return mTempC;
}
//---------------------------------------------------------------------------------------------------------------------
float Ccs811::temperatureF() const {
    return (mTempC * 9) / 5 + 32;
}
