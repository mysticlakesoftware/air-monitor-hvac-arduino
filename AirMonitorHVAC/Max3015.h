#ifndef MAX3015_H_
#define MAX3015_H_
//======================================================================================================================
// Max3015.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
/*
 * The ADC at maximum is 18 bits. This scales back to 15 bits.
 */

class Max3015 : public SensorAbstract {
public:
    enum Power { // any value to 0xFF works
        eNoPwr = 0, e4inch = 2, e8inch = 0x1F, e10inch = 0x7F, e12inch = 0xFF
    };
    enum LedMode {
        eRedOnly = 7, eRedIr = 2, eRedIrGrn = 3
    };
    enum LedPulse {
        e69Millis = 0, e188Millis, e215Millis, e411Millis
    // Corresponds to 15, 16, 17, 18 bit adc resolution
    // max values are 0x7FFF, 0xFFFF, 0x1FFFF, 0x3FFFF
    };
    enum SampleAvg {
        eNone = 1, eAvg2 = 2, eAvg4 = 4, eAvg8 = 8, eAvg16 = 16
    };
    enum SampleRate {
        e50 = 0, e100, e200, e400, e800, e1000, e1600, e3200
    };
    enum AdcRange {
        e2048 = 2048, e4096 = 4096, e8192 = 8192, e16384 = 16384
    };

    Max3015(const char* tag) :
        SensorAbstract(tag) {
    }
    virtual ~Max3015() {
    }

    bool begin(const Power pwr, const SampleAvg avg, const LedMode led, const SampleRate rate, const LedPulse pulse, const AdcRange range);

    bool isReady() override;
    virtual bool read() override;

    virtual void print() const override;

    float temperatureF() const;
    float temperatureC() const;

    uint32_t red() const;
    uint32_t ir() const;
    uint32_t green() const;

private:
    MAX30105 mMax30105;

    float mTempC { };
    uint32_t mRed { };
    uint32_t mIr { };
    uint32_t mGreen { };
};
//---------------------------------------------------------------------------------------------------------------------
inline bool Max3015::isReady() {
    if (mExists) {
        return mMax30105.safeCheck(50);
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Max3015::temperatureF() const {
    return mTempC * 9.0 / 5.0 + 32.0;
}
//---------------------------------------------------------------------------------------------------------------------
inline float Max3015::temperatureC() const {
    return mTempC;
}
//---------------------------------------------------------------------------------------------------------------------
inline uint32_t Max3015::red() const {
    return mRed;
}
//---------------------------------------------------------------------------------------------------------------------
inline uint32_t Max3015::ir() const {
    return mIr;
}
//---------------------------------------------------------------------------------------------------------------------
inline uint32_t Max3015::green() const {
    return mGreen;
}
//---------------------------------------------------------------------------------------------------------------------
inline void Max3015::print() const {
    SensorAbstract::print();
    Serial << temperatureF() << tab;
}
#endif /* MAX3015_H_ */
