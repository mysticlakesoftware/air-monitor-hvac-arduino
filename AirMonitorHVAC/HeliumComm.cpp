//======================================================================================================================
// Helium.cpp
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
#include <Helium.h>

#include "Board.h"

#include "HeliumComm.h"
//---------------------------------------------------------------------------------------------------------------------
HeliumComm::HeliumComm() :
    mHelium( &atom_serial), mChannel( &mHelium) {
}
//---------------------------------------------------------------------------------------------------------------------
bool HeliumComm::begin(const char* channel) {

    mHelium.begin(helium_baud_b9600);

    mStatus = mHelium.connect();

    if (mStatus == helium_status_OK) {
        int8_t result;
        mStatus = mChannel.begin(channel, &result);

        struct helium_info info;
        mStatus = mHelium.info( &info);
        mMacAddr = info.mac;
    }

    return (mStatus == helium_status_OK);
}
//---------------------------------------------------------------------------------------------------------------------
bool HeliumComm::send(const char* data) {
    int8_t result;
    mStatus = mChannel.send(data, strlen(data), &result);
    return (mStatus == helium_status_OK);
}
//---------------------------------------------------------------------------------------------------------------------
time_t HeliumComm::cur_time() {
    struct helium_info info;
    mStatus = mHelium.info( &info);
    return info.time;
}
//---------------------------------------------------------------------------------------------------------------------
void HeliumComm::report_status() const {
    if (helium_status_OK != mStatus) {
        Serial.print("Failed\t\t");

        switch (mStatus) {
            case helium_status_OK:
                Serial.print("helium_status_OK");
                break;
            case helium_status_OK_NO_DATA:
                Serial.print("helium_status_OK_NO_DATA");
                break;
            case helium_status_ERR_COMMUNICATION:
                Serial.print("helium_status_ERR_COMMUNICATION");
                break;
            case helium_status_ERR_NOT_CONNECTED:
                Serial.print("helium_status_ERR_NOT_CONNECTED");
                break;
            case helium_status_ERR_DROPPED:
                Serial.print("helium_status_ERR_DROPPED");
                break;
            case helium_status_ERR_KEEP_AWAKE:
                Serial.print("helium_status_ERR_KEEP_AWAKE");
                break;
            case helium_status_ERR_CODING:
                Serial.print("helium_status_ERR_CODING");
                break;
        }
        Serial.println();
    }
}
//---------------------------------------------------------------------------------------------------------------------
void HeliumComm::report_status_result() const {

    if (helium_channel_send_OK != mResult) {
        switch (mResult) {
            case helium_channel_send_OK:
                Serial.print("helium_channel_send_OK");
                break;
            case helium_channel_send_ERR_NOT_CONNECTED:
                Serial.print("helium_channel_send_ERR_NOT_CONNECTED");
                break;
            case helium_channel_send_ERR_DROPPED:
                Serial.print("helium_channel_send_ERR_DROPPED");
                break;
            case helium_channel_send_ERR_COMMUNICATION:
                Serial.print("helium_channel_send_ERR_COMMUNICATION");
                break;
            default:
                Serial.print("helium channel unknown result");
                break;
        }
        Serial.println();
    }
}
