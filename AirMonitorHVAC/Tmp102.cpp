//======================================================================================================================
// Tmp201.cpp
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
#include <Streaming.h>

#include "SparkFunTMP102.h"
#include "SensorAbstract.h"

#include "Tmp102.h"

//---------------------------------------------------------------------------------------------------------------------
Tmp102::Tmp102(const char* tag, const I2CAddr i2c_add) :
    SensorAbstract(tag), mI2cAddr { i2c_add }, mTmp102 { i2c_add } {
}
//---------------------------------------------------------------------------------------------------------------------
//  quick and dirty way to tell if there is a device at a I2C address
void Tmp102::begin() {
    mExists = true;
    Wire1.beginTransmission(mI2cAddr);
    mExists = (Wire1.endTransmission() == 0);
}
//---------------------------------------------------------------------------------------------------------------------
bool Tmp102::read() {
    mTempC = mTmp102.readTempC();
    mTmp102.sleep();
    return true;
}
