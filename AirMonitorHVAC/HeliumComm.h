#ifndef HELIUMCOMM_H_
#define HELIUMCOMM_H_
//======================================================================================================================
// Helium.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================

class HeliumComm {
public:
    HeliumComm();

    bool begin(const char* channel = "Mystic-Google-IOT");
    bool send(const char* data);

    time_t cur_time();  // current time from helium channel
    uint64_t mac_addr();

    bool isConnected();
    bool needsReset();
    int reset();
    int status() const;

    void report_status() const;
    void report_status_result() const;

private:
    Helium mHelium;
    Channel mChannel;
    int mStatus { };
    int mResult { };
    uint64_t mMacAddr { };
};
//---------------------------------------------------------------------------------------------------------------------
inline uint64_t HeliumComm::mac_addr() {
    return mMacAddr;
}
//---------------------------------------------------------------------------------------------------------------------
inline bool HeliumComm::isConnected() {
    return mHelium.connected();
}
//---------------------------------------------------------------------------------------------------------------------
inline bool HeliumComm::needsReset() {
    return mHelium.needs_reset();
}
//---------------------------------------------------------------------------------------------------------------------
inline int HeliumComm::reset() {
    return mHelium.reset();
}
//---------------------------------------------------------------------------------------------------------------------
inline int HeliumComm::status() const {
    return mStatus;
}

#endif /* HELIUMCOMM_H_ */
