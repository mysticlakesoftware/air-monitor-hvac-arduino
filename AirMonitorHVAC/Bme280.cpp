//======================================================================================================================
// Bme280.cpp
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================
#include <Streaming.h>
#include <SparkFunBME280.h>

#include "SensorAbstract.h"
#include "Bme280.h"
//----------------------------------------------------------------------------------------------------------------------
Bme280::Bme280(const char* tag, const I2CAddr i2c_addr) :
    SensorAbstract(tag),
    mI2cAddr { i2c_addr } {
    mBme280.settings.I2CAddress = i2c_addr;
}
//----------------------------------------------------------------------------------------------------------------------
bool Bme280::begin(const Mode mode, const Standby standby, const HumFilter hum, const OverSample temp, const OverSample prs,
    const OverSample humidity) {

    mBme280.reset();
    if (mBme280.begin()) {
        mExists = true;

        mBme280.setFilter(hum);
        mBme280.setStandbyTime(standby);
        mBme280.setMode(mode);

        mBme280.setTempOverSample(temp);
        mBme280.setPressureOverSample(prs);
        mBme280.setHumidityOverSample(humidity);
    }
    return mExists;
}
