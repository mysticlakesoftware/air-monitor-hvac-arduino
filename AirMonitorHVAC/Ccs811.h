#ifndef CCS811_H_
#define CCS811_H_
//======================================================================================================================
// Ccs811.h
//
// 2018 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//======================================================================================================================

//---------------------------------------------------------------------------------------------------------------------
class Ccs811 : public SensorAbstract {
public:
    enum I2CAddr { //
        CCS811_1 = 0x5B, CCS811_2 = 0x5A
    };

    enum Mode {
        idle = 0, OneSec, TenSec, SixtySec, MS250
    };

    Ccs811(const char* tag, const I2CAddr i2c_addr);

    void begin(const Mode mode = OneSec);

    bool isReady() override;
    bool isDataAvailable();
    virtual bool read() override;
    virtual void print() const override;

    void setEnvironmentalData(const float humidity, const float temp_c);

    /*
     * Readings in ppm and scaled to percentage of range of readings - not what is technically meant by atmospheric reporting.
     *
     * The CO2 scales from 400 to 8192
     * The VOC scales from 0 to 1187
     */

    uint16_t co2() const;
    uint16_t co2_scaled() const;

    uint16_t voc() const;
    uint16_t voc_scaled() const;

    float temperatureC();
    float temperatureF() const;

private:
    CCS811 mCcs811;
    uint16_t mCo2 { 0 };
    uint16_t mVoc { 0 };
    float mTempC { 0 };

};
//---------------------------------------------------------------------------------------------------------------------
inline Ccs811::Ccs811(const char* tag, const I2CAddr i2c_addr) :
    SensorAbstract(tag), mCcs811 { i2c_addr } {
}
//---------------------------------------------------------------------------------------------------------------------
inline bool Ccs811::isReady() {
    if (mExists) {
        return isDataAvailable();
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------

inline void Ccs811::setEnvironmentalData(const float humidity, const float temp_c) {
    mCcs811.setEnvironmentalData(humidity, temp_c);
}
//---------------------------------------------------------------------------------------------------------------------
inline uint16_t Ccs811::co2() const {
    return mCo2;
}
//---------------------------------------------------------------------------------------------------------------------
inline uint16_t Ccs811::voc() const {
    return mVoc;
}
//---------------------------------------------------------------------------------------------------------------------
inline uint16_t Ccs811::co2_scaled() const {
    if (mCo2 < 400) {
        return 0;
    }
    return round(float(mCo2 - 400) * 100.0 / (8192 - 400.0));
}
//---------------------------------------------------------------------------------------------------------------------
inline uint16_t Ccs811::voc_scaled() const {
    return round(float(mVoc) * 100.0 / 1187);
}
//---------------------------------------------------------------------------------------------------------------------
inline bool Ccs811::read() {
    return isReady();
}
//---------------------------------------------------------------------------------------------------------------------
inline void Ccs811::print() const {
    SensorAbstract::print();
    Serial << co2_scaled() << tab   //
        << voc_scaled() << tab   //
        << _FLOAT(temperatureF(), 1) << tab;
}

#endif /* CCS811_H_ */
